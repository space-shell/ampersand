# CSS/HTML Exercise

We've provided a design (the provided .psd and .png are identical), assets from the design and an index.html file.

To complete this exercise, please build out the component to match the designs as closely as possible. You are free to use whatever CSS tools you wish (SASS/LESS etc), but your final code should be fully viewable when opening the index.html file included in a browser.

You have 30 minutes to work through the CSS exercise, you don't need to worry about completing the entire task just get as far as you're able to in the time.
