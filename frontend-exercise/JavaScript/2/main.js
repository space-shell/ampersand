const names = [
    { firstname: 'Jon', lastname: 'Snow' },
    { firstname: 'Eddard', lastname: 'Stark' },
    { firstname: 'Gregor', lastname: 'Clegane' },
    { firstname: 'Cersei', lastname: 'Lannister' },
    { firstname: 'Melisandre' },
    { firstname: 'Daenerys', lastname: 'Targaryen' },
    { firstname: 'Drogo' },
    { firstname: 'Margaery', lastname: 'Tyrell' },
    { firstname: 'Tyrion', lastname: 'Lannister' },
    { firstname: 'Arya', lastname: 'Stark' },
    { firstname: 'Shae' },
    { firstname: 'Varys' }
]

const sortNames = ( names, order = 'a-z' ) =>
  names.sort
    ( ( { firstname: a }, { firstname: b } ) =>
        a >= b
          ? order === 'a-z' ? 1 : -1
          : order === 'a-z' ? -1 : 1
    )

console.log( sortNames( names ) )

console.log( sortNames( names, 'z-a' ) )
