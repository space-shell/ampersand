const names = [
    { firstname: 'Jon', lastname: 'Snow', titles: ['King in the North'] },
    { firstname: 'Eddard', lastname: 'Stark', titles: ['Lord of Winterfell', 'Warden of the North'] },
    { firstname: 'Gregor', lastname: 'Clegane', titles: ['Ser'] },
    { firstname: 'Cersei', lastname: 'Lannister', titles: ['Lady of Casterly Rock', 'Queen of the Andals and the First Men', 'Protector of the Seven Kingdoms'] },
    { firstname: 'Melisandre', titles: ['The Red Woman', 'The Red Witch'] },
    { firstname: 'Daenerys', lastname: 'Targaryen', titles: ['Mother of Dragons', 'Breaker of Chains'] },
    { firstname: 'Drogo', titles: ['Kahl'] },
    { firstname: 'Margaery', lastname: 'Tyrell', titles: ['Queen Consort'] },
    { firstname: 'Tyrion', lastname: 'Lannister', titles: ['Hand of the Queen'] },
    { firstname: 'Arya', lastname: 'Stark', titles: ['Princess'] },
    { firstname: 'Shae' },
    { firstname: 'Varys', titles: ['Master of Whisperers'] }
]

const sortNamesAndPrependTitles = ( names, order = 'a-z' ) => names
  .sort
    ( ( { firstname: a }, { firstname: b } ) =>
        a >= b
          ? order === 'a-z' ? 1 : -1
          : order === 'a-z' ? -1 : 1
    )
  .map
    ( ({ firstname, lastname, titles }) =>
        `
          ${
            titles 
              ? titles
                  .map
                    ( ( title, index, { length } ) => {
                      if (index === length - 1)
                        return title 

                      return index === length - 2
                        ? title + ' and'
                        : title + ','
                    })
                  .join(' ')
              : ''
          } \
          ${ firstname || '' } \
          ${ lastname || '' }
        `
          .trim()
          .replace(/\s+/g, ' ')
    )

console.log( sortNamesAndPrependTitles( names ) )
