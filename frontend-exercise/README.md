# Frontend Developer Interview

## JavaScript

You have 30 minutes to work through the JavaScript exercises, you don't need to worry about completing them all just get as far as you're able to in the time. You're free to use Google or any other resource/documentation you would normally use while programming.

Please keep your JavaScript as simple as possible, given the limited time available we're not looking for the use of any frameworks or build tools - vanilla JavaScript is perfect. We'll execute the JavaScript files via the CLI using the latest version of NodeJS, so you can use supported ES6 syntax if preferred.

Your JavaScript code should output the results to the log.

## CSS

You have 30 minutes to work through the CSS exercises, you don't need to worry about completing them all just get as far as you're able to in the time.

Assets have been provided for you in the `CSS/assets/` folder.

You are free to use whatever CSS tools you wish (SASS/LESS etc), but your final code should be fully viewable when opening the index.html file included in a browser.
